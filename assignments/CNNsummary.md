**What is CNN?**
CNN is a type of neural network. In layman its is the combination of mathematics , biology with some AI tossed in. It is based on the animal visual cortex.
It is used for classification , like if you wanna classify image in dog or cat.

**How CNN works?**
1. We accomplish this by observing the features of an image and store each feature in a fliter(matrix).
2. The we convolve that feature through the image to detect the particualr feature in the image.
3. Then we apply filter of color and classify it.
4. Then we get output images containing these features called feature maps.
5. From this fixed grid we take the max value and write it as single pixel value, this is called pooling.
6. Then last layer , the output layer , it gives the probability of each class. Usually softmax is used as activation function.

**kernel:** It is the size of sliding window in pixels.

**Strides:** It is the no of pixels the kernel window moves in each convolution step.

**Zero Padding:** Its just amount of zeros in edge of kernel window. It helps to completely filter the input image at edges.

**No of filter:** It is no of filter our convolutional layer have, it controls no of feature convolutional layer will look for.

**Flattening:** It brings all feature maps in single vector. 
